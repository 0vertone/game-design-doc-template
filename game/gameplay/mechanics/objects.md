Objects – how to pick them up and move them 


These can be as simple as flower pots or as advanced as spaceships.
You don't need a description for every item but you do need to write consistent rules that all objects in the game must follow. Can they move, do they follow a path, does each object have its own gravity or mass?
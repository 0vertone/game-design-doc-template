Structure - What is the structure of the game play? 


What are the mechanics that the player must use or overcome to progress. EG Puzzle Mechanics, Car Racing, Horron and Tension, etc...

NOTE: Mechanics goes into more detail later on so don't be too detailed in this regard. Also don't confuse this with genre. The term "horror game" means different things to different people. Be descriptive and use adjectives. EG Intensely realistic first person shooter with driving mechanics and bunnies.
Development hardware and software, including Game Engine.

What will you or the development team need in order to make the game?

EG Game Engine, 3d Rendering Toolkit, Sound Engine, Middleware, Peripheral Drivers.
Chipsets from reputable games company, VR headset, Workstation with latest graphics hardware, a good desk and a coffee machine.
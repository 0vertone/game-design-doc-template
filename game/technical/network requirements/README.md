Network Requirements - What is required in order for the player to access a network of other players or information. This could be as simple as a basic high scores server or a full infrastructure of online coop servers featuring AMQP protocols and headaches.

Use your best judgement when structuring this section
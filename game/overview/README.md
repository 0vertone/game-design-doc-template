Game Overview

At a glance, people should just be able to read this section and get a feel for the game you intend to create.
Don't go into too much detail. People will ask questions and should be able to find the answers in other sections.
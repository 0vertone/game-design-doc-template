Non-combat and Friendly Characters

The same rules apply to NPCS that apply to allies however there are some subtle difference. Animations behaviours and intelligence may all be different. Allies may be able to interact with objects but NPCS actively avoid them.
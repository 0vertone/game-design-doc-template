Movement - Objects and Characters in the game.

This will largely depend on the genre of game but don't be afraid to go into detail here. Does the player glide along the floor while shooting or do they have a realistic running animation with heavy breathing effects.

This section can also include any vehicles or other modes of transportation.
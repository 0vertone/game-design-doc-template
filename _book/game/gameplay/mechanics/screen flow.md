Screen Flow - A graphical description of how each screen is related to every other and a description of the purpose of each screen.


This depends on the genre of your game. If it is a side scroller with no right to left travel then this could be quite simple to do via loading screens for different scenes. Open world games may behave differently andhave some clever rendering capabilities baked into the engine.
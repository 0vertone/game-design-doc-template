Actions - Actions, including whatever switches and buttons are used, interacting with objects, and what means of communication are used.


Not all games will have these as options but even a jump can be considered an action. So can wall running and opening an inventory.
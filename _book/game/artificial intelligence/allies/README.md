Support AI -- Player and Collision Detection, Pathfinding

A decent game programmer will likely have some input into how these should operate. How does the player interact with friendly AI. Do players collide with friendlys via physics engine or pass through them without penalty.

Note: Each decision has consequences. Having support AI take up physical space can be dangerous due to them blocking key paths and generally annoying the player. Subsequently, having a player phase through a friendly AI can kill the immersion. 
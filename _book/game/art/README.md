Game Art – Key assets, how they are being developed. Intended style.

Feel free to link to hosted game art, put your art here or keep everything in the assets folder. 3d model files, sound files, video files and art assets which you need to give developers an impression should go here.
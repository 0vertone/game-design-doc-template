Game Design Doc Template
======

The following is a template document designed to be used as a base for other game design docs.
The template should be general themed enough to cover just all modern game genres. Although some sections may not
be necessary to include for certain types of game. EG Combat sections in a Narrative driven game.